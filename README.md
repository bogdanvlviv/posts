# Posts

2022-11-20 [Hello, World!](https://gitlab.com/bogdanvlviv/posts/-/issues/1)

2021-09-13 [Hello, GitLab](https://gitlab.com/bogdanvlviv/posts/-/issues/27)

2021-05-19 [SameSite cookies and Rails](https://gitlab.com/bogdanvlviv/posts/-/issues/26)

2021-01-03 [Ти знаєш, що ти - людина](https://gitlab.com/bogdanvlviv/posts/-/issues/25)

2020-06-09 [Power of iteration](https://gitlab.com/bogdanvlviv/posts/-/issues/24)

2020-05-20 [Code review](https://gitlab.com/bogdanvlviv/posts/-/issues/23)

2020-05-20 [Inside Rails: The Lifecycle of a Request/Response](https://gitlab.com/bogdanvlviv/posts/-/issues/22)

2019-11-11 [Rails Girls Lviv - 2019-12-07](https://gitlab.com/bogdanvlviv/posts/-/issues/21)

2019-03-22 [How to upgrade a big Rails application](https://gitlab.com/bogdanvlviv/posts/-/issues/20)

2019-03-01 [Rails Girls Lviv - 2019-03-30](https://gitlab.com/bogdanvlviv/posts/-/issues/19)

2019-01-14 [How to synchronize a directory between two remote hosts with rsync](https://gitlab.com/bogdanvlviv/posts/-/issues/18)

2018-11-22 [Rails Teens: день програмування для старшокласників - 2018-12-08 - м. Львів](https://gitlab.com/bogdanvlviv/posts/-/issues/17)

2018-10-19 [minitest-mock_expectations 1.0.0 released](https://gitlab.com/bogdanvlviv/posts/-/issues/16)

2018-10-08 [What is new in Rails 6.0](https://gitlab.com/bogdanvlviv/posts/-/issues/15)

2018-08-15 [Array#extract! to Active Support 6.0](https://gitlab.com/bogdanvlviv/posts/-/issues/14)

2018-08-07 [References to changes in Rails 5.2](https://gitlab.com/bogdanvlviv/posts/-/issues/13)

2018-03-13 [Ruby on Rails 5.2 Release Notes](https://gitlab.com/bogdanvlviv/posts/-/issues/12)

2018-02-10 [Rails Girls Lviv - 2018-02-10](https://gitlab.com/bogdanvlviv/posts/-/issues/11)

2017-11-24 [Pivorak 28: Rails 5.2](https://gitlab.com/bogdanvlviv/posts/-/issues/10)

2017-11-02 [How to install the latest tmux on Ubuntu 16.04](https://gitlab.com/bogdanvlviv/posts/-/issues/9)

2017-10-25 [How to install the latest Vim on Ubuntu 16.04](https://gitlab.com/bogdanvlviv/posts/-/issues/8)

2017-08-02 [... with `__dir__` we can restore order in the Universe](https://gitlab.com/bogdanvlviv/posts/-/issues/7)

2017-05-25 [New aliases "append" to Array#push, and "prepend" to Array#unshift (since Ruby 2.5.0)](https://gitlab.com/bogdanvlviv/posts/-/issues/6)

2017-05-11 [New method Kernel#yield_self (since Ruby 2.5.0)](https://gitlab.com/bogdanvlviv/posts/-/issues/5)

2017-03-12 [live-idea-2017: Ruby on Rails курс](https://gitlab.com/bogdanvlviv/posts/-/issues/4)

2016-12-18 [Design Patterns in Ruby](https://gitlab.com/bogdanvlviv/posts/-/issues/3)

2016-10-02 [Decorator Pattern in Ruby](https://gitlab.com/bogdanvlviv/posts/-/issues/2)
